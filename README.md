这些年来，一直不停的在写文章，很多时候有些知识点已经忘了是否写过，于是就导致了做了很多无用功。

这次我特意整理了Linux运维从零基础进阶的文章补录，后期我会把所有的目录对应的文章链接过来，这样就相当于你只要看这一篇文章，就可以系统的学习Linux运维了。

我会从下图中的知识点去写这个系列，每次完成一个系列后会整合这个行业里的大牛来分工录制视频。 视频录制好后，部分内容会发到B站。



黑色字体表示需要整理或者需要重新写的内容，蓝色字体点击可直接阅读！课表也有根据写的内容不断调整，直到做成史上最详细的运维开发入门手册。

如需要沟通，可以加微信zmgaosh  一起交流学习

 **第0阶段  学习方法篇
** 

 
-  [Linux运维学习方法篇](https://blog.csdn.net/xinshuzhan/article/details/102018544)

-  [90后，零基础学习运维，算晚吗？内含书单](https://www.zhihu.com/question/50957230/answer/1237667569)

- [你只是想学好Linux而已](https://blog.csdn.net/xinshuzhan/article/details/103259747)
-  [企业linux运维岗位详细介绍](https://blog.csdn.net/xinshuzhan/article/details/105040755)
- [运维项目流程](https://blog.csdn.net/xinshuzhan/article/details/89439383)
- [月薪12K的运维需要掌握的技术栈](https://blog.csdn.net/xinshuzhan/article/details/105881191)
-  [运维人需要具备哪些性格品质](https://blog.csdn.net/xinshuzhan/article/details/106178789)
-  [运维人员如何问问题](https://blog.csdn.net/xinshuzhan/article/details/102906017)
- [学Linux运维的前景-2017年手稿](https://blog.csdn.net/xinshuzhan/article/details/78221975)
- [优秀学员分享公司情况与学习方法](https://pan.baidu.com/s/1ejmJ9BWsMWB2HVzeqK2gpA)
    




 **第一阶段   linux基础** 

1.1 计算机组成原理及linux历史

1.2 vmware使用技巧

- [万字带图教程，带你从零开始安装虚拟机](https://blog.csdn.net/xinshuzhan/article/details/106822452)
- [【linux】循序渐进学运维-基础篇-vmware克隆虚拟机](https://blog.csdn.net/xinshuzhan/article/details/107248734)
- [实战： 克隆后的网络处理](https://blog.csdn.net/xinshuzhan/article/details/77413783)
- [实战：解决公司内不能分配桥接IP的情况](https://blog.csdn.net/xinshuzhan/article/details/107459630)

1.3  Linux 基础操作
- [CentOS7系统的安装](https://blog.csdn.net/xinshuzhan/article/details/102548217)
- [centos7实验环境的基本配置](https://blog.csdn.net/xinshuzhan/article/details/102557436)
- [Centos7解除自动锁屏及修改主机名](https://blog.csdn.net/xinshuzhan/article/details/78074871)
- [百度搜索及使用技巧](https://blog.csdn.net/xinshuzhan/article/details/106867048)
- [【linux】循序渐进学运维-基础篇-linux运维7个级别](https://zmedu.blog.csdn.net/article/details/106915865)

1.4  Linux 企业中常用命令详解

- [循序渐进学运维-基础命令篇](https://blog.csdn.net/xinshuzhan/article/details/102615542)
- [循序渐进学运维-ls](https://blog.csdn.net/xinshuzhan/article/details/102712446)
- [循序渐进学运维-echo](https://blog.csdn.net/xinshuzhan/article/details/102717850)
- [一个让rm-rf 都头大的命令](https://blog.csdn.net/xinshuzhan/article/details/106526575)
- [[linux] 循序渐进学运维-mv](https://blog.csdn.net/xinshuzhan/article/details/103112940)
- [【linux】循序渐进学运维-mkdir](https://blog.csdn.net/xinshuzhan/article/details/103092864)
- [循序渐进学运维-cp](https://blog.csdn.net/xinshuzhan/article/details/102882849)
- [循序渐进学运维-wc](https://blog.csdn.net/xinshuzhan/article/details/102877514)
- [循序渐进学运维-printf](https://blog.csdn.net/xinshuzhan/article/details/102788793)
- [循序渐进学运维-基础命令篇-查找类命令](https://blog.csdn.net/xinshuzhan/article/details/106842929)
- [循序渐进学运维-cat](https://blog.csdn.net/xinshuzhan/article/details/102809064)
- [find命令](https://blog.csdn.net/xinshuzhan/article/details/103259342)


1.5  VIM的使用技巧

- [vim从入门到放弃-绝密版](https://blog.csdn.net/xinshuzhan/article/details/106858796)
- [vim技巧提高篇](https://blog.csdn.net/xinshuzhan/article/details/77414675)
- [diff命令的使用](https://zmedu.blog.csdn.net/article/details/106863257)


1.6  linux系统目录及系统用户

- [linux系统目录详解](https://blog.csdn.net/xinshuzhan/article/details/106891159)
- [【linux】循序渐进学运维-基础篇-Linux文件管理命令](https://blog.csdn.net/xinshuzhan/article/details/106892441)
- [管理用户和组](https://blog.csdn.net/xinshuzhan/article/details/106871055)
- [实战： 学会这个骚操作，再也不怕从删库到跑路](https://zmedu.blog.csdn.net/article/details/106864492)


1.7  文件权限管理


- [【linux】循序渐进学运维-基础篇-文件权限管理](https://blog.csdn.net/xinshuzhan/article/details/106879055)
- [【linux】循序渐进学运维-基础篇-特殊权限管理-SUID等](https://blog.csdn.net/xinshuzhan/article/details/106894795)


1.8  软件包管理

- [[linux]循序渐进学运维-基础命令篇-文件的归档和压缩](https://editor.csdn.net/md?articleId=106875973)
- [常见ssh管理工具](https://blog.csdn.net/xinshuzhan/article/details/106942385)
- [【linux】循序渐进学运维-基础篇-rpm管理](https://blog.csdn.net/xinshuzhan/article/details/106899800)
- [yum包的管理及配置](https://blog.csdn.net/xinshuzhan/article/details/106907724)
- [阿里云安装软件镜像源](https://blog.csdn.net/xinshuzhan/article/details/77414696)

- [实战： Linux上安装jdk](https://blog.csdn.net/xinshuzhan/article/details/77414691)

1.9  系统进程管理

-[【linux】循序渐进学运维-基础篇-进程管理](https://blog.csdn.net/xinshuzhan/article/details/106921249)

- [【linux】循序渐进学运维-基础篇-top命令](https://blog.csdn.net/xinshuzhan/article/details/106944616)
- 虚拟文件系统/proc  /sys
- [kill命令管理](https://blog.csdn.net/xinshuzhan/article/details/106950079)
- [【运筹帷幄】网站打开慢故障排查思路](https://blog.csdn.net/xinshuzhan/article/details/107076193)


2.0  硬盘详解

- 常见存储设备介绍
- [【linux】循序渐进学运维-基础篇-分区命令fdisk](https://blog.csdn.net/xinshuzhan/article/details/106954319)
- [使用fdisk管理分区](https://blog.csdn.net/xinshuzhan/article/details/104516621)
- [【linux】循序渐进学运维-基础篇-修复文件系统实战](https://blog.csdn.net/xinshuzhan/article/details/107073523)
- [【linux】循序渐进学运维-基础篇-mount](https://blog.csdn.net/xinshuzhan/article/details/106956602)
- [硬盘加密技术](https://blog.csdn.net/xinshuzhan/article/details/107137784)
- [阿里云服务器安装后无swap解决方案](https://blog.csdn.net/xinshuzhan/article/details/104518068)


1.11   文件系统

- EXT文件系统结构
- [【linux】循序渐进学运维-基础篇-文件的软硬链接](https://blog.csdn.net/xinshuzhan/article/details/107093821)
- [硬盘文件系统常用命令](https://blog.csdn.net/xinshuzhan/article/details/107136163)
- [实战： 修复公司服务器文件系统详解](https://blog.csdn.net/xinshuzhan/article/details/107073523)
- [性能监控工具-glances](https://blog.csdn.net/xinshuzhan/article/details/77987300)

1.12 高性能存储管理及raid

- [【linux】循序渐进学运维-基础篇-磁盘阵列](https://blog.csdn.net/xinshuzhan/article/details/107007830)
- [【Linux】raid管理工具-mdadm-raid0管理](https://blog.csdn.net/xinshuzhan/article/details/104536112)
- [【linux】RAID磁盘阵列介绍](https://blog.csdn.net/xinshuzhan/article/details/104535214)
- [【linux】mdadm-raid1管理](https://blog.csdn.net/xinshuzhan/article/details/104537487)


1.13  LVM及磁盘配额技术

- [LVM原理精讲](https://blog.csdn.net/xinshuzhan/article/details/107529760)
- [lvm的创建及使用](https://blog.csdn.net/xinshuzhan/article/details/107534030)


1.14  计划任务和日志管理

- [【linux】循序渐进学运维-基础篇-at命令](https://blog.csdn.net/xinshuzhan/article/details/107028569)
- [企业cron定制周期性计划任务](https://blog.csdn.net/xinshuzhan/article/details/107145138)
- [日志切割及日志轮询](https://blog.csdn.net/xinshuzhan/article/details/104555974)
- 实战： [rsyslog实现企业级日志其中管理](https://blog.csdn.net/xinshuzhan/article/details/104559325)
- [实战： 企业级日志管理及备份案例分享](https://blog.csdn.net/xinshuzhan/article/details/104559325)
- [实战： 如何让历史记录不记录敏感命令](https://blog.csdn.net/xinshuzhan/article/details/77414641)

1.15 linux系统启动原理及故障排除

- [【linux】循序渐进学运维-基础篇-Linux系统启动原理](https://blog.csdn.net/xinshuzhan/article/details/107149127)


1.16  linux网络详解

- [【linux】循序渐进学运维-基础篇-netstat命令详解](https://blog.csdn.net/xinshuzhan/article/details/107280435)

- [【linux】循序渐进学运维-基础篇-配置静态IP详解](https://blog.csdn.net/xinshuzhan/article/details/107273279)

   第一阶段补充内容：
 - [【Linux】sudo分权管理实战](https://blog.csdn.net/xinshuzhan/article/details/107583992)

 **第二阶段  shell编程** 

shell基础

- shell的作用
- shell脚本开发的基本规范与习惯
- shell开发环境的配置及自动化技巧
- shell开发的20条规范总结
- shell 变量及变量类型
- read交互与算数运算
- 实战： 通过shell脚本监控mysql邮件报警多案例详解
- 实战： 一键安装及批量添加服务器案例
- 实战： 分析web日志IP，pv案例（阿里面试题）
- [实战：猜数字游戏-学员版本](https://blog.csdn.net/xinshuzhan/article/details/103740766)

shell进阶

- shell正则表达式
- shell高级命令
- shell条件循环语句详解
- shell函数详解
- [实战： 开发实现keepalived高可用及健康检查脚本](https://blog.csdn.net/xinshuzhan/article/details/89439191)
- [实战：LVS客户端自动配置脚本](https://blog.csdn.net/xinshuzhan/article/details/89439301)
- 实战： 实现主从同步检测脚本
- 实战： 实现日志分析与备份脚本

- [常用shell脚本集锦](https://blog.csdn.net/xinshuzhan/article/details/91419087)

shell高级

- 三剑客详解
- 无交互shell脚本案例编写
- 实战： 公司带宽猛涨分析及shell解决方案
- [实战： shell日常巡检脚本](https://blog.csdn.net/xinshuzhan/article/details/89439206)
- 实战： 遭受木马后，通过shell脚本编写代码还原案例
- [使用函数封装-公司自动化处理文件备份管理脚本](https://blog.csdn.net/xinshuzhan/article/details/77414677)

python脚本

- [域名过期提前通知脚本](https://blog.csdn.net/xinshuzhan/article/details/77414679)

 **第三阶段  linux常见服务实战** 

3.1  SSHD服务

- [企业服务器实验环境搭建详解](https://blog.csdn.net/xinshuzhan/article/details/107324394)
- [【Linux】循序渐进学运维-服务篇-telnet](https://blog.csdn.net/xinshuzhan/article/details/107320959)
- [常见的SSH工具介绍](https://blog.csdn.net/xinshuzhan/article/details/106942385)
- [SSHD基本配置](https://blog.csdn.net/xinshuzhan/article/details/107326759)
- [【Linux】循序渐进学运维-服务篇-ssh配置文件详解](https://blog.csdn.net/xinshuzhan/article/details/107330007)
- [sshd实现秘钥认证](https://blog.csdn.net/xinshuzhan/article/details/107332697)
- [【Linux】循序渐进学运维-服务篇-SCP命令](https://blog.csdn.net/xinshuzhan/article/details/107342704)
- [实战： 使用fail2ban解决暴力破解问题](https://blog.csdn.net/xinshuzhan/article/details/103259832)
- [实战：阿里云ssh远程连接短时间就会断掉的解决方案](https://blog.csdn.net/xinshuzhan/article/details/107273525)
- [实战： sudo分权管理案例](https://blog.csdn.net/xinshuzhan/article/details/107583992)

3.2  rsync实战
- [【Linux】循序渐进学运维-服务篇-rysnc原理](https://blog.csdn.net/xinshuzhan/article/details/107348464)
- [【Linux】循序渐进学运维-服务篇-rysnc安装及使用](https://blog.csdn.net/xinshuzhan/article/details/107358328)
- [【Linux】循序渐进学运维-服务篇-rsync配置文件](https://blog.csdn.net/xinshuzhan/article/details/107366103)
- [【Linux】循序渐进学运维-服务篇-rsync实战](https://blog.csdn.net/xinshuzhan/article/details/107367717)
- [inotify 部署及应用](https://blog.csdn.net/xinshuzhan/article/details/107376334)
- [实战： rsync+inotify实现同步与监控](https://blog.csdn.net/xinshuzhan/article/details/107399556)
- [实战：【Linux】 NFS服务器实现开机自动挂载](https://blog.csdn.net/xinshuzhan/article/details/107434659)

3.3  FTP与DHCP

- DHCP协议原理详解
- DHCP常见选项配置及使用
- [【Linux】循序渐进学运维-服务篇-FTP的原理及使用](https://blog.csdn.net/xinshuzhan/article/details/107410284)
- [【Linux】循序渐进学运维-服务篇-FTP服务配置文件详解](https://blog.csdn.net/xinshuzhan/article/details/107417377)
- [【Linux】lftp客户端使用详解](https://blog.csdn.net/xinshuzhan/article/details/107427558)

3.4  DNS服务

- DNS原理解析
- DNS体系结构
- BIND 实现正向解析与反向解析
- [实战： 搭建主从DNS服务器](http://mp.weixin.qq.com/s?__biz=MzIzNjI5MzYxMA==&mid=2247484777&idx=1&sn=f658075364159306ba35aecdf0d2ab7a&scene=6#wechat_redirect)

3.5  apache

- [【Linux】apache服务相关概念及安装](https://blog.csdn.net/xinshuzhan/article/details/107444152)
- [apache服务器配置文件详解](https://blog.csdn.net/xinshuzhan/article/details/107585413)
- [【Linux】apache虚拟主机实战](https://blog.csdn.net/xinshuzhan/article/details/107622905)
- [【Linux】apache的认证授权和访问控制](https://blog.csdn.net/xinshuzhan/article/details/107637156)
- [【Linux】ab命令实现网站性能压力测试](https://blog.csdn.net/xinshuzhan/article/details/107595123)
- [CA认证详解及认证的搭建过程](https://blog.csdn.net/xinshuzhan/article/details/104673402)
- [基于apahce 实现https](https://blog.csdn.net/xinshuzhan/article/details/104674300)
- [【Linux】LAMP架构安装及安装论坛](https://blog.csdn.net/xinshuzhan/article/details/107593070)
- [企业常用名词UV，pv，ip深度讲解](https://blog.csdn.net/xinshuzhan/article/details/107645081)
- [apache与tomcat整合实现动静分离](http://mp.weixin.qq.com/s?__biz=MzIzNjI5MzYxMA==&mid=2247484781&idx=1&sn=e601b8d67a73c2332f2789a8b628bfc6&scene=6#wechat_redirect)
- 实战： [【Linux】手把手教你搭建自己个人博客（boss版）](https://blog.csdn.net/xinshuzhan/article/details/107603441)
- 实战： [【Linux】宝塔堡垒机上线网站初体验](https://blog.csdn.net/xinshuzhan/article/details/107601852)
- [实战： 在阿里云上部署仿知乎网站](https://blog.csdn.net/xinshuzhan/article/details/107548642)
- [实战： 仿知乎网站的后台管理](https://blog.csdn.net/xinshuzhan/article/details/107552797)
- [【linux】apache的访问日志详解及演练](https://blog.csdn.net/xinshuzhan/article/details/107638752)
-  linux日志切割
- [【linux】 apache多后缀文件解析漏洞复现](https://blog.csdn.net/xinshuzhan/article/details/107647037)


3.6 tomcat

- [【Linux】循序进阶学运维-服务篇-tomcat入门](https://blog.csdn.net/xinshuzhan/article/details/107725083)
- [【Linux】循序渐进学运维-服务篇-与tomcat相关的概念](https://blog.csdn.net/xinshuzhan/article/details/107725114)
- [【Linux】循序渐进学运维-tomcat配置文件详解](https://blog.csdn.net/xinshuzhan/article/details/107725450)

3.7  nginx

- [【Linux】循序渐进学运维-服务篇-nginx入门](https://blog.csdn.net/xinshuzhan/article/details/107701836)
- [【Linux】循序渐进学运维-CentOS7使用yum方式安装nginx](https://blog.csdn.net/xinshuzhan/article/details/107705300)
- [【linux】循序渐进学运维-服务篇-nginx的虚拟主机](https://blog.csdn.net/xinshuzhan/article/details/107708283)
- [搭建高负载web服务器架构LNMP](https://blog.csdn.net/xinshuzhan/article/details/107718306)



3.8  zabbix/prometheus
- [zabbix的简单介绍](https://blog.csdn.net/xinshuzhan/article/details/80074590)
- [centos7安装zabbix3.4]( https://blog.csdn.net/xinshuzhan/article/details/78075518 )
- [【linux】centos7下使用LAMP架构实现zabbix 4.0部署](https://blog.csdn.net/xinshuzhan/article/details/107726302)
- [zabbix监控服务器](https://blog.csdn.net/xinshuzhan/article/details/80077613)
- [zabbix基本使用-用户信息](https://blog.csdn.net/xinshuzhan/article/details/80077751)
- [zabbix微信告警](https://blog.csdn.net/xinshuzhan/article/details/80090961)
- 实战：docker中zabbix 监控系统构建
- [prometheus的介绍及安装](https://blog.csdn.net/xinshuzhan/article/details/105156582)
- [prometheus图形界面的基本监控配置](https://blog.csdn.net/xinshuzhan/article/details/105183425)
- [prometheus服务监控之mysql监控](https://blog.csdn.net/xinshuzhan/article/details/105186534)
- [Grafana+prometheus实现主机监控](https://blog.csdn.net/xinshuzhan/article/details/105191017)
- [生产环境下搭建 nagios+nconf+cacti+npc的整合](https://blog.csdn.net/xinshuzhan/article/details/77414676)

3.9  SVN

- [【linux】循序渐进学运维-服务篇-svn服务器单机实战](https://blog.csdn.net/xinshuzhan/article/details/107742649)
- [实战 SVN+apache 服务器搭建北](https://blog.csdn.net/xinshuzhan/article/details/107743773)

- [svn基础命令应用](https://blog.csdn.net/xinshuzhan/article/details/107744064)


4.0  自动化运维工具

- ansible
- saltstack
- puppet

 **第四阶段  mysql DBA进阶** 

初级
- [centos7安装MySQL5.7](https://blog.csdn.net/xinshuzhan/article/details/84728966)
- [【mysql】新手必备 centos7 安装mysql8](https://blog.csdn.net/xinshuzhan/article/details/107497495)
- mysql基础入门
- mysql多实例及企业应用场景
- mysql增删改查
- [MySQL-sql语句进阶](https://blog.csdn.net/xinshuzhan/article/details/102827635)
- 实战：搭建LAMP环境部署及DZ论坛
- [实战： Linux环境下MySQL的root密码忘记的解决办法](https://blog.csdn.net/xinshuzhan/article/details/77414640)

中级

- mysql备份恢复实战
- mysql主从同步
- mysql读写分离
- mysql常见数据结构
- mysql集群及代理
-[【mysql】mysql调优时必须掌握的慢查询语句排查命令](https://blog.csdn.net/xinshuzhan/article/details/107482405)

高级

- mysql存储过程
- [MySQL的外键约束](https://blog.csdn.net/xinshuzhan/article/details/102850437)
- [MySQL索引](https://blog.csdn.net/xinshuzhan/article/details/102846009)
- mysql存储引擎
- mysql数据结构
- mysql日常运维及监控
- [pt工具的介绍（ptf-quey-digest查询工具](https://blog.csdn.net/xinshuzhan/article/details/80701205)
- mysql日常压力测试sysbench
- mysql数据库优化思想及实战
- [【mysql】mysql调优时必须掌握的慢查询语句排查命令](https://blog.csdn.net/xinshuzhan/article/details/107482405)
- mysql业务流变更及安全管理思想
- [MySQL面试题 ](https://blog.csdn.net/xinshuzhan/article/details/89439125)

 **第五阶段   linux系统安全及调优** 

linux安全
- [linux基础优化](https://blog.csdn.net/xinshuzhan/article/details/104694546)
- [常见的调优命令](https://blog.csdn.net/xinshuzhan/article/details/104676189)
- [修改Linux swap空间的swappiness 降低对硬盘的缓存](https://blog.csdn.net/xinshuzhan/article/details/80660223)

- iptables
- 系统用户安全相关配置及管理
- linux服务相关的安全
- l[inux网络安全之信息收集](https://blog.csdn.net/xinshuzhan/article/details/103259828)
- linux防火墙及动态防火墙技术
- linux攻击种类及分析
- web脚本漏洞分析
- SQL注入漏洞分析
- 漏洞检测攻击使用
- 暴力破解解决方案
- 日志服务，远程攻击
- tcpwrappers服务器安全加固
- 加密认证介绍
- CA认证过程详解及阿里云CA认证
- HTTP与https详解
- [CA认证及搭建过程](https://blog.csdn.net/xinshuzhan/article/details/104673402)
- [基于apache实现https](https://blog.csdn.net/xinshuzhan/article/details/104674300)
- [实战： 网站被入侵后的抓虫技巧](https://blog.csdn.net/xinshuzhan/article/details/77414680)
- snort入侵检测攻击搭建技巧
- openvpn服务器的搭建
- [nmap扫描工具详解](https://blog.csdn.net/xinshuzhan/article/details/78200287)
- linux木马程序rootkit隐藏行踪实战
- linux黑客提权详解及破解方案
- kali linux使用方法
- DB安全防护方案（运维层面）
- 实战： 加密解密解决中间人攻击
- 实战: DDOS攻击实战及防范策略
- 实战： [Jumpserver 跳板机基于阿里云实战](https://blog.csdn.net/xinshuzhan/article/details/103869601)

 **第六阶段   linux服务集群** 

应用系统架构

- 架构设计原理详解（4个原则）
- 集群概念及企业应用场景
- [redis介绍](https://blog.csdn.net/xinshuzhan/article/details/80446196)
- [varnish 介绍与安装实战](https://blog.csdn.net/xinshuzhan/article/details/104751363)
- keepalived原理及工作流程
- 基于HAproxy七层负载均衡的搭建
- 基于LVS四层负载实战
- 基于nginx搭建七层负载均衡
- 使用heartbeat实现web高可用架构
- 使用ldirectord检测realserver服务器状态
- 脑裂问题出现原因及解决方案
- [WebVirMgr的搭建过程](https://blog.csdn.net/xinshuzhan/article/details/89439501)


- 企业服务器架构解决方案
- [架构项目](https://blog.csdn.net/xinshuzhan/article/details/99619745)
- 期中考试架构详解

 **第七阶段  虚拟化实战** 

7.1 虚拟化

- ESXI
- Vcenter实战
- 常见桌面虚拟化技术
- [KVM虚拟化](https://blog.csdn.net/xinshuzhan/article/details/103757116)

 **7.2 公有云** 

- 阿里云
- 腾讯云
- 各云平台选购测试方案

 **7.3 OpenStack** 

- M版本基于红帽的自动化安装
- [packstack快速部署open stack](https://blog.csdn.net/xinshuzhan/article/details/103889548)
- O版本Keystone及glance环境详解
- 网络模型与Neutron介绍
- 日志与监控实战

 **7.4 docker** 

- docker原理及安装使用
- [centos7 安装docker](https://blog.csdn.net/xinshuzhan/article/details/84701620)
- docker镜像容器与仓库
- [docker pull 下载加速-2018](https://blog.csdn.net/xinshuzhan/article/details/103259739)
- [docker安装mysql实战](https://blog.csdn.net/xinshuzhan/article/details/84702740)
- [实战：私有仓库实战](https://blog.csdn.net/xinshuzhan/article/details/103259746)
- docker虚拟化网络及应用
- [docker容器ssh连接](https://blog.csdn.net/xinshuzhan/article/details/79041068)
- docker持久化管理
- docker三剑客
- docker日常运维实战
- 实战： 基于docker的CI/CD
- 实战： 流水线自动化发布PHP项目实战
- 实战： 流水线自动化发布JAVA项目实战
- 实战： 基于K8S的企业级CI/CD方案

 **7.5 容器编排Kubernetes** 

- [k8s简介和安装](https://blog.csdn.net/xinshuzhan/article/details/107525401)
- [kubernetes踩坑-单点集群的安装](http://mp.weixin.qq.com/s?__biz=MzIzNjI5MzYxMA==&mid=2247484720&idx=1&sn=fd372cc0c42f1c79b0e1eede0f33a37f&scene=6#wechat_redirect)
- [从KubeOperator开启k8s之旅](https://blog.csdn.net/xinshuzhan/article/details/103225248)
- k8s节本概念和操作
- 容器的运维和监控
- docker+devops实战

 **第八阶段   DevOps实战** 

 8.1 ELK实战

- ELK stack介绍
- ELK Stack架构详解
- [Elasticsearch基础概念](https://blog.csdn.net/xinshuzhan/article/details/104059556)
- [ELK的搭建](https://blog.csdn.net/xinshuzhan/article/details/89439564)
- [隔壁老王的女朋友都能学会的ELK实战系列之kibana](https://blog.csdn.net/xinshuzhan/article/details/105783063)
- [隔壁老王的女朋友都能学会的ELK实战之elasticsearch](https://blog.csdn.net/xinshuzhan/article/details/105781876)
- Elasticsearch集群部署
- Elasticsearch 数据库增删改查
- Head插件图形化管理
- Logstash input常用插件
- Logstash Codec常用插件
- Kibanna数据展示
- 基于nginx实现Kibanna访问认证
- 引入redis
- 引入Fllebeat
- 实战： 收集Java堆栈日志
- 实战：收集nginx日志
- 实战： 定制日志格式收集
- 实战：kibana可视化及仪盘表

8.2 jenkins
- [【DevOps】软件开发生命周期](https://blog.csdn.net/xinshuzhan/article/details/107160807)
- [【DevOps】持续集成](https://blog.csdn.net/xinshuzhan/article/details/107178477)
- [jenkins-php代码发布图](https://blog.csdn.net/xinshuzhan/article/details/89439538)

- [【DevOps】持续集成的流程及jenkins的介绍](https://blog.csdn.net/xinshuzhan/article/details/107197037)
- [【DevOps】持续集成环境-Jenkins安装图文版](https://blog.csdn.net/xinshuzhan/article/details/107209792)
- [【devops】持续集成环境-jenkins插件管理](https://blog.csdn.net/xinshuzhan/article/details/107453224)
- [jenkins安装配置](https://blog.csdn.net/xinshuzhan/article/details/80115881)
- Jenkins job环境准备
- Jenkins  shell集成和参数集成
- [maven的安装和介绍](https://blog.csdn.net/xinshuzhan/article/details/101689020)
- Jenkins+maven+git+ansible集成

8.3 gitlab

- [【DevOps】centos7 下的 gitlab托管服务器的介绍与安装](https://blog.csdn.net/xinshuzhan/article/details/107201884)
- gitlab工作流程
- gitlab安装配置管理
- gitlab应用

8.4 ansible

- ansible介绍
- ansible的优势及使用场景
- ansibel配合virtualenv安装配置
- playbooks入门及规范
- ansible常用模块介绍

实战：Jenkins+ansible+gitlab解决自动化部署持续交付问题



mesos整体架构技术介绍

marathon实战

 **第九阶段： 大数据相关产品** 
- [haoop的相关介绍及概念](https://blog.csdn.net/xinshuzhan/article/details/104610125)
- [hadoop-分布式存储工作原理及名词解释](https://blog.csdn.net/xinshuzhan/article/details/104611485)
- [大数据离线分析工具HIve介绍](https://blog.csdn.net/xinshuzhan/article/details/80382153)

 **第十阶段   面试真题&& 面经分享** 

 面试真题：

- [最不起眼的基础知识，却成了你面试跨不过去的门槛](https://blog.csdn.net/xinshuzhan/article/details/106804939)
- [高薪offer，只因做对了这套笔试题](https://blog.csdn.net/xinshuzhan/article/details/106761542)
- [阿里云公司笔试题答案 ](https://blog.csdn.net/xinshuzhan/article/details/106222712)
- [redis的面试题](https://blog.csdn.net/xinshuzhan/article/details/102989934)
- [运维常见面试题汇总-2019无答案版本](https://blog.csdn.net/xinshuzhan/article/details/101748671)

面试及职场答疑：

- [（职场答疑） 干了一个月运维很迷茫怎么办](https://www.zhihu.com/question/22974176/answer/1265239724)
- [（答疑）面试完一直让等offer怎么办？](https://mp.weixin.qq.com/s/U-rtraOXzwNBQsVnDYBOqQ)
- [（答疑） 收到入职通知，但一直不谈薪资也不发offer要不要去](https://www.zhihu.com/question/398915239/answer/1260353341)
- [（答疑）面试被拒后，hr又打电话来问是否还愿意去上班，该如何回复](https://www.zhihu.com/question/397395440/answer/1255272583)
- [给刚入职的学员的新人手册](https://blog.csdn.net/xinshuzhan/article/details/101625586)
- [（答疑）专科生转行做python运维靠谱吗](https://www.zhihu.com/question/318710852/answer/1198519678)- [（答疑）有什么经验教训，是你在面试很多次之后才知道的？](https://blog.csdn.net/xinshuzhan/article/details/106204338)

 **过关斩将系列** 

 [ 面试高频问题的详细解说](https://gitee.com/gaosh08/Linux_Interview/blob/master/interview.md)


 **日常生活** 

 ** 福利** (目前还在整理，整理完后会统一通知）
 - Linux运维的学习路线
 - 整理的书单（附个人喜欢的书）
 -[工作13年，这些私藏的使用工具/学习网站我都贡献出来了](https://blog.csdn.net/xinshuzhan/article/details/106867048)
 - 整理好用的工具集
 - 通用的学习方法
 - 电子书（请勿传播）
 - 面试资料
 - 简历模板



自我介绍：
- 公众号：【大数据架构师专家】
- csdn: [高胜寒](https://blog.csdn.net/xinshuzhan)
- B站：[https://space.bilibili.com/290497083](https://space.bilibili.com/290497083) （后续持续更新）
- 直播：[http://live.bilibili.com/9226672](http://live.bilibili.com/9226672) （后续会陆续直播）
- Gitee: [https://gitee.com/gaosh08/LinuxFamily](https://gitee.com/gaosh08/LinuxFamily)


